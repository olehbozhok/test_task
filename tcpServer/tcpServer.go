package tcpServer

import (
	"fmt"
	"net"
	"os"
	"time"

	"gitlab.com/olehbozhok/test_task/sortedsetStrings"
	"bufio"
)

const (
	CONN_TYPE = "tcp"
)

func RunServer(laddr string) {
	l, err := net.Listen(CONN_TYPE, laddr)
	if err != nil {
		fmt.Println("tcp server error listening:", err.Error())
		os.Exit(1)
	}
	defer l.Close()

	fmt.Println("tcp server is listening on " + laddr)
	for {
		// Listen for an incoming connection.
		conn, err := l.Accept()
		if err != nil {
			fmt.Println("Error accepting: ", err.Error())
			os.Exit(1)
		}
		// Handle connections in a new goroutine.
		go handleRequest(conn)
	}
}

func handleRequest(conn net.Conn) {
	conn.SetDeadline(time.Now().Add(time.Second * 1))


	scanner := bufio.NewScanner(conn)
	scanner.Split(bufio.ScanWords)

	for scanner.Scan() {
		scanner.Text()
		sortedsetStrings.Add(scanner.Text())
	}
	// end reading
	//if err := scanner.Err(); err != nil {
	//	fmt.Fprintln(os.Stderr, "reading input:", err)
	//}

	conn.Write([]byte("OK"))
	conn.Close()
}
