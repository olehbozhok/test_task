package webserver

import (
	"net/http"

	"encoding/json"
	"github.com/labstack/echo"
	"strconv"

	"github.com/wangjia184/sortedset"
	"gitlab.com/olehbozhok/test_task/sortedsetStrings"
	"regexp"
)

//I don't know what sense in this re
var wordRE = regexp.MustCompile(`[\S]+`)

func Run(address string) {
	e := echo.New()
	e.GET("/", func(c echo.Context) error {
		countString := c.QueryParam("count")
		reString := c.QueryParam("re")

		countInt64, err := strconv.ParseInt(countString, 10, 64)
		count := int(countInt64)
		if countString != "" && countString != "all" && err != nil {
			return c.String(http.StatusOK, "Error parse count")
		}

		if countString == "" {
			count = 10
		} else if countString == "all" {
			count = -1
		}

		sortedNodes := sortedsetStrings.GetSorted(count)
		if len(sortedNodes) == 0 {
			return c.String(http.StatusOK, "Nothing to show.")
		}

		if reString != "" {
			var tmpNodes []*sortedset.SortedSetNode
			for _, node := range sortedNodes {

				if wordRE.MatchString(node.Key()) {
					tmpNodes = append(tmpNodes, node)
				}
			}
			sortedNodes = tmpNodes
		}
		respJson, err := json.MarshalIndent(sortedNodes, "", "  ")
		if err != nil {
			return c.String(http.StatusOK, "Error marshal json:"+err.Error())
		}
		return c.JSONBlob(http.StatusOK, respJson)
	})

	e.Logger.Fatal("run webserver error: ", e.Start(address))
}
