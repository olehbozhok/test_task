package main

import (
	"gitlab.com/olehbozhok/test_task/tcpServer"
	"gitlab.com/olehbozhok/test_task/webserver"
)

func main()  {
	go tcpServer.RunServer("localhost:9000")
	// http://localhost:8000/?count=all&re=1
	webserver.Run(":8000")
}