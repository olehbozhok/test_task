package sortedsetStrings

import (
	"github.com/wangjia184/sortedset"
	"strings"
	"github.com/anacrolix/sync"
)
var replacer = strings.NewReplacer(",", "", ".", "","\"","")

var set = sortedset.New()

var rwMutex = sync.RWMutex{}

func Add(key string) (score sortedset.SCORE) {

	key = strings.TrimSpace( replacer.Replace(key) )
	lastElement := set.GetByKey(key)
	rwMutex.Lock()
	defer rwMutex.Unlock()
	if lastElement != nil {
		score = lastElement.Score() + 1
		set.AddOrUpdate(key, score, map[string]interface{}{
			"key":   key,
			"score": score,
		})
	} else {
		score = 1
		set.AddOrUpdate(key, 1, map[string]interface{}{
			"key":   key,
			"score": score,
		})

	}

	return score
}

func GetSorted(limit int) (sortedNodes []*sortedset.SortedSetNode)  {
	rwMutex.RLock()
	defer rwMutex.RUnlock()
	if limit == -1 {
		limit = set.GetCount()
	}
	maxNode := set.PeekMax()
	if maxNode == nil {
		return sortedNodes
	}
	sortedNodes = set.GetByScoreRange(maxNode.Score()+1, 0,
		&sortedset.GetByScoreRangeOptions{
			ExcludeEnd:   true,
			ExcludeStart: true,
			Limit:        limit,
		})
	return 	sortedNodes
}